const Button = ({ color, title, onClick, isBlock }) => {
    return (
        <button
            style={{ backgroundColor: color }}
            className={`btn ${isBlock ? 'btn-block' : ''}`}
            onClick={onClick}
        >
            {title}
        </button>
    )
}

Button.defaultProps = {
    title: 'Submit',
    color: '#111',
}

export default Button
