import Button from './Button'
import { useState } from 'react'

const AddTask = ({ onAdd }) => {
    const [title, setTitle] = useState('')
    const [date, setDate] = useState('')
    const [reminder, setReminder] = useState(false)

    const onSubmit = (event) => {
        event.preventDefault()

        if (!title) {
            alert('Please enter a title for your task.')
            return
        }

        onAdd({ title, date, reminder })

        setTitle('')
        setDate('')
        setReminder(false)
    }

    return (
        <form className="add-form">
            <div className="form-control">
                <label>Task Title</label>
                <input
                    type="text"
                    placeholder="Enter Title"
                    value={title}
                    onChange={({ target }) => setTitle(target.value)}
                />
            </div>
            <div className="form-control">
                <label>Date</label>
                <input
                    type="text"
                    placeholder="Enter Due Date"
                    value={date}
                    onChange={({ target }) => setDate(target.value)}
                />
            </div>
            <div className="form-control form-control-check">
                <label>Set Reminder</label>
                <input
                    type="checkbox"
                    checked={reminder}
                    onChange={({ currentTarget }) =>
                        setReminder(currentTarget.checked)
                    }
                />
            </div>

            <Button onClick={onSubmit} isBlock={true} />
        </form>
    )
}

export default AddTask
