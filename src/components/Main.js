import { useState } from 'react'
import Header from './Header'
import Tasks from './Tasks'
import AddTask from './AddTask'

const Main = () => {
    const [formShow, setFormShow] = useState(false)

    const [tasks, setTask] = useState([])

    const addTask = (task) => {
        let max = 0
        tasks.forEach((task) => (task.id > max ? (max = task.id) : max))
        max++

        task.id = max

        setTask([...tasks, task])
    }

    const toggleReminder = (id) => {
        setTask(
            tasks.map((task) =>
                task.id === id
                    ? {
                          ...task,
                          reminder: !task.reminder,
                      }
                    : task
            )
        )
    }

    const deleteTask = (id) => {
        setTask(tasks.filter((task) => task.id !== id))
    }

    const toggleForm = () => {
        setFormShow(!formShow)
    }

    return (
        <div className="container">
            <Header onShowForm={toggleForm} formStatus={formShow} />
            {formShow && <AddTask onAdd={addTask} />}
            {tasks.length > 0 ? (
                <Tasks
                    tasks={tasks}
                    onDelete={deleteTask}
                    onToggle={toggleReminder}
                />
            ) : (
                <h5>No Tasks to Show.</h5>
            )}
        </div>
    )
}

export default Main
