import Button from './Button'

const Header = ({ onShowForm, formStatus }) => {
    return (
        <header className="header">
            <h1>Task Tracker</h1>
            <Button
                title={formStatus ? 'Close' : 'Add Task'}
                color={formStatus ? '#777' : 'green'}
                onClick={onShowForm}
                isBlock={false}
            />
        </header>
    )
}

export default Header
